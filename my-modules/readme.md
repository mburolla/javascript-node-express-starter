# Modules
- ECMA (JavaScript) vs OpenJS (Node)
- [CommonJS (NodeJS)](https://www.sitepoint.com/understanding-module-exports-exports-node-js/)
- [ECMAScript (ES Modules) React](https://nodejs.org/api/esm.html#modules-ecmascript-modules)

# CommonJS (NodeJS)
- This is the default configuration for NodeJS
- `require`
- `exports` is a shortcut to `module.exports`

# ECMAScript (React)
- This is the default configuration for React apps
- `import`
- `export` (Notice: no "s" here)

# Land of Confusion
Authors can tell Node.js to use the ECMAScript module loader via the `.mjs` file extension, the `package.json` "type" field, or the --input-type flag. Outside of these cases, Node.js uses the CommonJS module loader. 

Add the following to `package.json` to force Node.js to use the ECMAScript module loader (`import` statements):
```
"type": "module"
```

Rant: This is poorly named!  How about:
```
"moduleLoader" : {"ECMAScript"|"CommonJS"}
```
