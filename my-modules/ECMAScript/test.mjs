//
// File: test.mjs
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: ECMAScript Module (ES Module)
//

import { area } from './circle.mjs';

console.log(`The area of a circle with a radius of 4 is ${area(4)}.`);
