//
// File: square.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: CommonJS module
//

module.exports = class Square {
    constructor(width) {
      this.width = width;
    }
  
    area() {
      return this.width ** 2;
    }
};
