//
// File: calculator.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: CommonJS module
//

exports.add = (a, b) => a + b;

exports.add3 = (a, b, c) => {
    return a + b + c;
}
