//
// File: circle.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: CommonJS module
//

const { PI } = Math;

exports.area = (r) => PI * r ** 2;

exports.circumference = (r) => 2 * PI * r;

