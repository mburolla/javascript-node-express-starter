//
// File: test.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: CommonJS module
//

const circle = require('./circle.js');
const Square = require('./square.js');
const Calculator = require('./calculator.js');

const mySquare = new Square(2);

console.log(`The area of mySquare is ${mySquare.area()}.`);
console.log(`The area of a circle with a radius of 4 is ${circle.area(4)}.`);
console.log(`1 + 2 is: ${Calculator.add(1, 2)}.`);
console.log(`1 + 2 + 3 is: ${Calculator.add3(1, 2, 3)}.`);

