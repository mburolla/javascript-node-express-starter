//
// File: circle.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: Module
//

const { PI } = Math;

export const area = (r) => PI * r ** 2;

export const circumference = (r) => 2 * PI * r;
