//
// File: test.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: Module
//       

import { area } from './circle.js';  // NOTE: Node does not support this format by default, we have to change the filename or update package.json.

console.log(`The area of a circle with a radius of 4 is ${area(4)}.`);
