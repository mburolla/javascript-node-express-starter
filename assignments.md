# Assignments
Node/DB/API assignments.

# Ex. 1
Write a Node program that uses the Moment library to print the following on the console:

```
The date today is: 03/30/2022.
```

# Ex. 2
Write a Node program that creates a file called `dates.txt` which contains a list of 10 days from todays date (3/30/2022).  Use the Moment library.

Example (dates.txt):
```
03/30/2022
03/31/2022
04/01/2022
04/02/2022
04/03/2022
04/04/2022
04/05/2022
04/06/2022
04/07/2022
04/08/2022
```

# Ex. 3
Write a Node program that uses `axios` to make a GET call to: `https://jsonplaceholder.typicode.com/users`.  The program parses the names of people from this API call and displays it on the console:

Example output:
```
[
  'Leanne Graham',
  'Ervin Howell',
  'Clementine Bauch',
  'Patricia Lebsack',
  'Chelsey Dietrich',
  'Mrs. Dennis Schulist',
  'Kurtis Weissnat',
  'Nicholas Runolfsdottir V',
  'Glenna Reichert',
  'Clementina DuBuque'
]
```

# Ex. 4
Create a module called `json-placeholder-proxy.js` that exports a method called: `getAlbumsForUserId()`.  This method calls two API endpoints using `Axios`:

- http://jsonplaceholder.typicode.com/users/{userId}
- https://jsonplaceholder.typicode.com/albums

`getAlbumsForUserId()` returns a list similar to the following:

Example output:
```
[
  {
    id: 1,
    userId: 1,
    title: 'quidem molestiae enim',
    userName: 'Leanne Graham'
  },
  {
    id: 2,
    userId: 1,
    title: 'sunt qui excepturi placeat culpa',
    userName: 'Leanne Graham'
  },
  {
    id: 3,
    userId: 1,
    title: 'omnis laborum odio',
    userName: 'Leanne Graham'
  },
  {
    id: 4,
    userId: 1,
    title: 'non esse culpa molestiae omnis sed optio',
    userName: 'Leanne Graham'
  },
  {
    id: 5,
    userId: 1,
    title: 'eaque aut omnis a',
    userName: 'Leanne Graham'
  },
  {
    id: 6,
    userId: 1,
    title: 'natus impedit quibusdam illo est',
    userName: 'Leanne Graham'
  },
  {
    id: 7,
    userId: 1,
    title: 'quibusdam autem aliquid et et quia',
    userName: 'Leanne Graham'
  },
  {
    id: 8,
    userId: 1,
    title: 'qui fuga est a eum',
    userName: 'Leanne Graham'
  },
  {
    id: 9,
    userId: 1,
    title: 'saepe unde necessitatibus rem',
    userName: 'Leanne Graham'
  },
  {
    id: 10,
    userId: 1,
    title: 'distinctio laborum qui',
    userName: 'Leanne Graham'
  }
]

```

This module is consumed by a client script in the following manner:

```
const jsonplaceholderProxy = require('./json-placeholder-proxy');

const main = async () => {
    console.log(await jsonplaceholderProxy.getAlbums(1));
}

main();
```


# Ex. 5
Create a module called `file-parser.js` that parses the following input file:

```
3/30/2022,5
3/31/2022,10
4/1/2022,15
4/2/2022,5
4/3/2022,20
```
The module exports a function called `getNumberOfPhoneCalls()` which sums the number for each day.
This module is consumed by a client script in the following manner:

```
const fileParser = require('./file-parser.js');

const main = async () => {
   console.log(await fileParser.getNumberPhoneCalls());
}

main();
```

Example output:
``
55
``

Try to use `createReadStream()` and Promises for this assignment.

# Ex. 6
Create the following data model using MySQL Workbench:

A bookstore has a name, location and hours of operation.  A bookstore is run by one and only one manager.  A bookstore has many employees.  An employee has a first name, last name, date of birth, hire date and termination date.  A bookstore has many books.  A book has a title, author name, price and ISBN.

# Ex. 7
Create the following data model using MySQL Workbench:

A gym has a name, location and hours of operation. A gym has many members.  A member has a first name, last name and activation date.  Members of the gym have to check in AND check out.  Members can review the check in and checkout status and see how long they have been in the gym.  A member cannot enter the gym if they have not paid their monthly subscription.  A member subscribes to a monthly subscription plan.  A gym can have many monthly subscriptions plans (example $20/month, $30/month, $40/month).

# Ex. 8
Create an Express API that connects to a MySQL database that provides a simple CRUD wrapper for a Car resource.  The API supports the following endpoints:

```
GET /Car (Gets all cars)
GET /Car/{id}
PUT /Car/{id} (Update)
POST /Car/ (Create)
DELETE /Car/{id}

```

A Car has the following properties:
- CarId
- Make
- Model
- Year
- Color
- Price

# Ex. 9
Create an Express API that connects to a MongoDB database that provides a simple CRUD wrapper for an Engine (car engine) resource.  The API supports the following endpoints:

```
GET /Engine (Gets all engines)
GET /Engine/{id}
PUT /Engine/{id} (Update)
POST /Engine/ (Create)
DELETE /Engine/{id}

```

An Engine has the following properties:

- EngineId
- CarId
- Size
- Cylinders
- Horsepower

# Ex. 10
Create an Express API that contains the following endpoint:

```
GET /Car/{id}
```

This endpoint returns all the data for this car from the relational database and MongoDB database from the previous exercises.

Example output:
```
{
    "car_id": 1,
    "make": "Toyota",
    "year": 2022,
    "color": "Gray",
    "price": 30000,
    "engine" : {
         "engine_id": 1,
         "size": 2.0,
         "cylinders: 8, 
         "horsepower": 400
    }
}

```
