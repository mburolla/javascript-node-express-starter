const EventEmitter = require('events');

const myEmitter = new EventEmitter();

myEmitter.emit('TEST'); // Oops, we published an event before we subscribed.

// Subcribe
myEmitter.on('TEST', () => {
    console.log('test')
});

myEmitter.on('TEST2', () => {
    console.log('test2')
});

// Publish
myEmitter.emit('TEST');
myEmitter.emit('TEST');
