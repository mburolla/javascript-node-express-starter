//
// File: client.js
// Auth: Martin Burolla
// Date: 3/28/2022
// Desc: Tests our Express API
//

import axios from 'axios';

const main = async () => {
    let r = await axios.get('http://localhost:5150/person');
    console.log(r.data);

    let p = { firstName: "Joe", lastName: "Smith"};
    r = await axios.post('http://localhost:5150/person', p);
    console.log(r.data);
}

main();
