/**
 * Schema
 */
CREATE TABLE `person` (
  `person_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cat` (
  `cat_id` int NOT NULL AUTO_INCREMENT,
  `person_id` int DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `fk_person_id_idx` (`person_id`),
  CONSTRAINT `fk_person_id` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/**
 * Data
 */
insert into person (first_name, last_name) values ("Alice", "Chains");
insert into person (first_name, last_name) values ("Bob", "Marley");
insert into person (first_name, last_name) values ("Charlie", "Horse");
insert into person (first_name, last_name) values ("Dave", "Jones");

insert into cat (person_id, `name`, color) values (1, "Rocky", "White");
insert into cat (person_id, `name`, color) values (2, "Baxie", "Gray");
insert into cat (person_id, `name`, color) values (3, "Gypsy", "Black");
insert into cat (person_id, `name`, color) values (3, "Gabby", "Brown");
insert into cat (person_id, `name`, color) values (3, "Tom", "Brown");
insert into cat (person_id, `name`, color) values (4, "Bruce", "Gray");
insert into cat (person_id, `name`, color) values (4, "Blazie", "Black");
insert into cat (person_id, `name`, color) values (4, "Binky", "Gray");
insert into cat (person_id, `name`, color) values (4, "Batcat", "Black");
