//
// File: mysql-proxy.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: Wrapper around MySql database.
//

require('dotenv').config();
const mysql = require('mysql2');

const SELECT_PERSON = "select * from person where person_id = ?";
const SELECT_CATS_FOR_PERSON = `
    select * from person p
        join cat c on p.person_id = c.person_id
    where p.person_id = ?;
    `;

const pool = mysql.createPool(
    {
        host: process.env.DB_HOST, 
        user: process.env.DB_USER, 
        password: process.env.DB_PWD, 
        database: process.env.DB_NAME
    });
const promisePool = pool.promise();

exports.getPerson = async (id) => {
    let rows = null;
    [rows] = await promisePool.query(SELECT_PERSON, [id]);
    return rows;
};

exports.getCatsForPersonId = async (id) => {
    let rows = null;
    [rows] = await promisePool.query(SELECT_CATS_FOR_PERSON, [id]);
    return rows;
};
