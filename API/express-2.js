// 
// File: express-2.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: Simple API with open CORS config and data access.
//

const cors = require('cors');
const mysql = require('mysql2');
const express = require('express');

const PORT = 5150;
const app = express();

var corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200
}

// Middleware...
app.use(express.json());
app.use(express.urlencoded());
app.use(cors());

app.get('/person/:id', cors(corsOptions), async (req, res) => {
    let rows = null;
    try {
        const pool = mysql.createPool({host:'127.0.0.1', user: 'db-user', password: 'Ihp5150!', database: 'express'});
        const promisePool = pool.promise();
        [rows] = await promisePool.query("select * from person where person_id > ?", [req.params.id]);
    }
    catch(e) {
        console.log(e)
    }
    res.send(rows);
});

app.listen(PORT, () => {
    console.log(`Express Server is running on port: ${PORT}`);
});
