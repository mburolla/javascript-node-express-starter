# Run the API in development
- `nodemon express-1.js`

# Run the API in Production
- `pm2 start express-1.js`
- `pm2 stop {id}`
- `pm2 list`
- `pm2 monit`

# CORS
- `npm install cors -S`
- `app.use(cors());`
- [Configuration](https://expressjs.com/en/resources/middleware/cors.html)

# APIs
- Can return HTML templates
- Can return JSON data (this is probably more common)
