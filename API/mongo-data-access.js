//
// File: mongo-data-access.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: Mongo db CRUD wrapper.
//       https://www.npmjs.com/package/mongodb
//

const { MongoClient } = require('mongodb');

const DB_NAME = 'MartyDB';
const COLLECTION = 'Books';
const URL = 'mongodb://localhost:27017';
const CLIENT = new MongoClient(URL);

//
// Public
//

//
// Inserts
//

exports.insertBook = async (book) => {
  const collection = await createCollection();
  return await collection.insertOne(book);
}

exports.insertManyBooks = async (books) => {
  const collection = await createCollection();
  return await collection.insertMany(books);
}

//
// Select
//

exports.findBookByIsbn = async (isbn) => {
  const collection = await createCollection();
  return await collection.find({isbn}).toArray();
}

exports.findAllBooks = async () => {
  const collection = await createCollection();
  return await collection.find({}).toArray(); 
}

//
// Update
//

exports.updateBookTitleForIsbn = async (isbn, newTitle) => {
  const collection = await createCollection();
  return await collection.updateOne({isbn: isbn}, {$set: {title: newTitle}});
}

//
// Delete
//

exports.deleteBookByIsbn = async (isbn) => {
  const collection = await createCollection();
  return await collection.deleteOne({isbn});
}

exports.deleteAllBooksForAuthor = async (authorName) => {
  const collection = await createCollection();
  return await collection.deleteMany({author: authorName});
}

//
// Private
//

const createCollection = async () => {
  await CLIENT.connect();
  const db = CLIENT.db(DB_NAME);
  return db.collection(COLLECTION);
}
