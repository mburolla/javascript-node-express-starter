# What is Node?
- JavaScript on backend servers
- Asynchronous APIs w/o worrying about managing threads
- Node Package Manager (NPM)
- Platform for tools
- JavaScript engine: V8 Node is a V8 Wrapper

# What is a Callback?
A callback is a function that gets executed at a later point in time.
Passing a function pointer to an asynchronous method will invoke the callback
function at a later point in time. For example:

```
const finishedCallback = () => {
    console.log('Finished');
}

someAsyncMethod(finishedCallback);
```

# What is a Promise?
A Promise is like a callback, but it Promises to return a successful "thing"
or an unsuccessful thing.  Success is resolve, failure is reject.

# REPL
- Read Evaluation Print Loop
- To enter, type: `node`
- To exit: `ctl+d`
- It's handy, but is not a major part of my workflow

# Modules
- ECMA Script modules end with `.mjs` 
  - `import http from 'http'`;
  - Named import: `import { createServer } from 'http';`

- CommonJS (Node) modules end with `.js`
  - `const http = require('http');`

 # Enviroment Variables
 - `process.env`

 # NodeMon
 - Development tool only used for development
 - `npm install nodemon`
 - Execute: `nodemon <my node script>`
