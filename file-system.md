# File System
- fs Module
  - Favor Streams (`createReadStream` & `createWriteStream`) over files
  - Streams are much more efficient

