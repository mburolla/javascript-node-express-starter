# Modules & Concurrency
- Modules are wrapped by a wrapper function
- Event Loop
  - The hidden process that manages asynchronous actions so we
  do not have to worry about managing threads.
- Try/Catch for specific errors:
- CommonJS: js `require()`
- ECMAScriptModules: mjs `import()`
- https://nodejs.org/api/modules.html

- The CommonJS (CJS) format is used in Node.js and uses require and `module.exports` to define dependencies and modules. The npm ecosystem is built upon this format.

- The ES Module (ESM) format. As of ES6 (ES2015), JavaScript supports a native module format. It uses an `export` keyword to export a module’s public API and an import keyword to import it.

- https://reflectoring.io/nodejs-modules-imports/

```
try {
    // Do some work...
} catch (err) {
    if (err.code === ???) {
        console.log(<appropiate message>);
    } else {
        throw err;
    }
}
```
- PM2 or Forever to manage long lived node scripts
- Error first callback pattern can results in "callback hell" no reason to do this any more
- `promisify` turns any built in async method (async/await)

# Events
- Streams are Event Emitters (stdin, stdout)
- Pub/Sub model
- Decouples code allowing modules to communicate with each other
without a formal API
- We can subscribe to the same event and do DIFFERENT things in different areas of our code


