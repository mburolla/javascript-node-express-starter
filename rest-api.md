# ReST API
Representation State Transfer (ReST) is a software architectural style that was created to guide the design and development of the architecture for the World Wide Web. ReST defines a set of constraints for how the architecture of an Internet-scale distributed hypermedia system, such as the Web, should behave. 

# Videos
- [What is a ReST API (9m)](https://youtu.be/lsMQRaeKNDk)
- [What is a Web API (7m)](https://youtu.be/_7rT-ixivWU)
- [You Give ReST a Bad Name (3m)](https://youtu.be/nSKp2StlS6s)

# Links
- [ReSTful API Design](https://florimond.dev/en/posts/2018/08/restful-api-design-13-best-practices-to-make-your-users-happy/)
