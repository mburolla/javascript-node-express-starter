# SQL & Data Modeling
The Structured Query Language (SQL) is the standard language for relational database management systems. SQL statements are used to perform tasks such as update data on a database, or retrieve data from a database.

Example:
```
select * from person;
```
Returns all the people stored in a table called people.

# Videos
- [Introduction to SQL (30m)](https://youtu.be/p3qvj9hO_Bo)
- [Joins (10m)](https://youtu.be/zGSv0VaOtR0)
- [Foreign Keys in MySQL (11m)](https://youtu.be/4JhXRll-jkQ)

# Links
- [What is an ERD?](https://www.lucidchart.com/pages/er-diagrams)
