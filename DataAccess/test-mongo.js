//
// File: test-mongo.js
// Auth: Martin Burolla
// Date: 3/31/2022
// Desc: Mongo DB tester.
//

const mongoDataAccess = require('./mongo-data-access.js'); // CommonJS module.

const main = async () => {
    // Find a Book.
    let r = await mongoDataAccess.findBookByIsbn(123);

    // Find all Books.
    //let r = await mongoDataAccess.findAllBooks();

    // Insert Book.
    //let r = await mongoDataAccess.insertBook({isbn: 777, title: "Invest in Yourself", author: "Rich Jones"});

    // Insert Many books
    // let books = [
    //     {isbn: 123, title: "Lucky Tom", author: "Mary Smith"},
    //     {isbn: 456, title: "How BGS Started", author: "Martin James Burolla"},
    //     {isbn: 789, title: "Learn How to Play Guitar", author: "Sam Reti"}
    // ]
    // let r = await mongoDataAccess.insertManyBooks(books);

    // Update title for existing Book.
    //let r = await mongoDataAccess.updateBookTitleForIsbn(123, "How BGS started")
    
    // Delete Book.
    //let r = await mongoDataAccess.deleteBookByIsbn(777);

    // Delete many Books.
    //let r = await mongoDataAccess.deleteAllBooksForAuthor("Me");

    console.table(r)
    process.exit();
}

main();
