//
// File: test-rdb.js
// Auth: Martin Burolla
// Date: 3/31/2022
// Desc: CRUD for Person.
//

const rdbDataAccess = require('./rdb-data-access.js'); // CommonJS module.

const main = async () => {
    // Select a Person.
    let r = await rdbDataAccess.selectPersonById(4);

    // Insert a Person.
    // let r = await rdbDataAccess.insertPerson( "Joe", "Green");

    // Update People.
    // let r = await rdbDataAccess.updatePerson( "Joe", "Joey");

    // Delete Person.
    // let r = await rdbDataAccess.deletePerson(16);

    console.table(r);
    process.exit();
}

main();
