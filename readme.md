# JavaScript Node Express Starter
Assignment repo for Node and Express API applications.  Assignments are [here](assignments.md).

# Getting Started
- Clone this repo
- Install dependencies: `npm install`

# Videos
[Node.js: Getting Started (3h 30m)](https://app.pluralsight.com/library/courses/nodejs-getting-started/table-of-contents)
- Event loop
  - [The Complete Node js: The Node js Event Loop (11m)](https://youtu.be/6YgsqXlUoTM) 
  - [The Node.js Event Loop: Not So Single Threaded (31m)](https://youtu.be/zphcsoSJMvM)

# Links
- [events.js](events.js)
- [Deep Copy/Shallow Copy/Spread](spread-deep-shallow-copy.mjs)

# Contents
- [Overview](overview.md)
- [NPM](packages.md)
- [Modules & Concurrency](mod-concur.md)
- [File System](file-system.md)
- [SQL & Data Modeling](sql-data-modeling.md)
- [ReST API](rest-api.md)

# Functions
Functions are "first class citizens" in JavaScript.  We can pass functions as arguments to other functions.  Function pointers are references to functions.

`requestListener` is a pointer to a function (AKA function pointer):

```
const requestListener = (req, res) => { // <== Anonymous function referenced by: requestListener.  
    res.end('Hello World\n');
}

const server = http.createServer(requestListener); // <== Not: requestListener()
```

# Debugging Node Scripts
- node --inspect-brk <YOUR SCRIPT>
- Browser: chrome://inspect (Click inspect link)
