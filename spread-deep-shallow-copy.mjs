//
// File: spread-deep-shallow-copy.mjs
// Auth: Martin Burolla
// Date: 3/23/2022
// Desc: Illustrates the spread operator, deep copy, shallow copy.
//

import _ from 'lodash';

const copyPrimativeTypes = () => {
    let a = [1,2,3]; // <== All primative types here.
    let b = [...a];

    b[0]= 5150;

    console.log(a);
    console.log(b);
}

const shallowCopy = () => {
    let p = { firstName: "Joe"}
    let a = [[p], [2], [3]];
    let b = [...a]; // <== Reference types are shallow copied.

    b[0][0].firstName = "Mary"

    console.log(a);
    console.log(b);
}

const deepCopy = () => {
    let p = { firstName: "Joe"}
    let a = [[p], [2], [3]];
    let b = _.cloneDeep(a); // <== DEEP COPY.

    b[0][0].firstName = "Mary"

    console.log(a);
    console.log(b);
}

///////////////////////////////////////////////////////////////////////////////////
// Topics
///////////////////////////////////////////////////////////////////////////////////

copyPrimativeTypes();
shallowCopy();
deepCopy();
