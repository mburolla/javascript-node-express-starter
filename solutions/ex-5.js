//
// File: ex-5.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: 
//

const fileParser = require('./file-parser.js'); // CommonJS

const main = async () => {
   console.log(await fileParser.getNumberPhoneCalls());
}

main();
