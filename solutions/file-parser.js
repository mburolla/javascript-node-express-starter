//
// File: file-parser.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: Parses a file and sums the number of phone calls for each day.
//

const fs = require('fs');

exports.getNumberPhoneCalls = async () => {
    let retval = 0;
    try {
        let fileContents = await readFile('./input.txt');
        let lineItemArray = fileContents.split('\r\n');
        let callCountArray = lineItemArray.map(i => parseInt(i.split(',')[1]));
        retval = callCountArray.reduce((ps, b) => ps + b, 0);
    }
    catch(e) {
        console.log(e);
    }
    return retval;
}

const readFile = () => {
    return new Promise((resolve, reject) => {
        let retval = "";
        const rs = fs.createReadStream('./input.txt');
        rs.on("error", reject);
        rs.on("data", chunk => retval += chunk);
        rs.on("end", () => resolve(retval)); // Return (resolve) only when we are done.
    });
}
