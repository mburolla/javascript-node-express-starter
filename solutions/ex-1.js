//
// File: ex-1.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: 
//

var moment = require('moment'); // CommonJS

const main = () => {
    moment.locale('de');
    console.log(`The date today is: ${moment().format('MM/DD/YYYY')}.`);
}

main();
