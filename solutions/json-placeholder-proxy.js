//
// File: json-place-holder-proxy.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: 
//

const axios = require('axios'); // CommonJS

exports.getAlbumsForUserId = async (userId) => {
    let retval = null;

    // Get the username for this userId.
    let username = (await axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`)).data.name;

    // Get all the albums.
    let allAlbums = await axios.get(`https://jsonplaceholder.typicode.com/albums`)

    // Return our result.
    let userAlbums = allAlbums.data.filter(i => i.userId === userId);
    retval = userAlbums.map(i => { 
        return { 
            id: i.id,
            userId: i.userId,
            title: i.title,
            userName: username 
        }
    });
    return retval;
}
