//
// File: ex-3.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: 
//

const axios = require('axios'); // CommonJS
// import axios from 'axios'; // ECMAScript

const main = async () => {
    let r = await axios.get('https://jsonplaceholder.typicode.com/users')
    let names = r.data.map(i => i.name);
    console.log(names);
}

main();
