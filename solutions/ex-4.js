//
// File: ex-4.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: 
//

const jsonplaceholderProxy = require('./json-placeholder-proxy');

const main = async () => {
    console.log(await jsonplaceholderProxy.getAlbums(1));
}

main();
