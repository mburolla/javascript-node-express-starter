//
// File: ex-2.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: 
//

const fs = require('fs');
var moment = require('moment'); // CommonJS

const main = () => {
    let stream = fs.createWriteStream('dates.txt', {flags: 'w'});
    for (let i = 0; i < 10; i++) {
        stream.write(`${moment().add(i, 'days').format('MM/DD/YYYY')}\r\n`);
    }
}

main();
